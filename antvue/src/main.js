import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from 'vue-router';
import Login from './common/Login.vue';
import axios from 'axios';

// 系统服务
import Home from './common/Home.vue'
import Role from './common/Role.vue'
import User from './common/User.vue'
// 财务管理
import financial from './control/financial.vue'
import freight from './control/freight.vue'
import remaining from './control/remaining.vue'
//运力管理
import Line from './transport/Line.vue'
import Rece from './transport/Rece.vue'
//引入生成二维码插件
import QRCode from "qrcode";

//基础配置
import Authentication from './basic/Authentication.vue'
import Information from './basic/Information.vue'
import Security from './basic/Security.vue'


axios.defaults.withCredentials = true;//让ajax携带cookie
Vue.prototype.axios = axios;
Vue.prototype.QRCode = QRCode;
Vue.use(ElementUI);
Vue.use(VueRouter);
const routes = [
  { path: "/", component: Login },
  {
    path: "/Home", component: Home,
    children: [
      {
        path: "/Home",
        name: "系统管理",
        component: Home,
        children: [{
          path: "/User",
          component: User,
          name: "用户管理"
        },
        { path: "/Role", component: Role, name: "角色管理" }
        ]
      },
      {
        path: "/Home",
        name: "基础配置",
        component: Home,
        children: [
          {
            path: "/Information",
            name: "公司资料",
            component: Information
          },
          {
            path: "/Authentication",
            name: "资质认证",
            component: Authentication
          },
          {
            path: "/Security",
            name: "安全中心",
            component: Security
          },
          {
            path: "/Home",
            name: "财务管理",
            component: Home,
            children: [
              {
                path: "/financial",
                name: "财务报表",
                component: financial
              },
              {
                path: "/freight",
                name: "运费收入",
                component: freight
              },
              {
                path: "/remaining",
                name: "余额提现",
                component: remaining
              }
            ]
          }
        ]
      }
    ]
  },{
    path: "/Home", component: Home,
    children: [
      {
        path: "/Line",
        name: "线路管理",
        component: Line,
      },{
        path: "/Rece",
        name: "客户管理",
        component: Rece,
      }
    ]
  }
]

const router = new VueRouter({
  routes
})
Vue.prototype.axios = axios;


//路由的钩子函数 表示路由之间的跳转必须经过此函数
router.beforeEach((to, from, next) => {
  console.log("要跳转的路径未：---->", to.path);
  if (to.path == '/') {
    localStorage.removeItem('username');
    next();
  } else {
    if (!localStorage.getItem('username')) {
      next("/");
      return;
    }
    //请求后台查询登录用户对应的菜单
    initMenu(router);
    setTimeout(() => {
      next();
    }, 500);
  }
});

export const initMenu = (router) => {
  axios({
    url: "http://localhost:9999/basic/tmsMenu/getMenuByUserName",
    method: "get",
    params: { "access_token": localStorage.getItem("access_token") }
  }).then(function (param) {
    console.log("根据当前登录用户的id动态获取权限菜单:{0}", param);
    if (param) {
      //因为后台返回的是一个List 里面的component 不是vue 识别的组件，因此需要把List 转换成Vue 识别的对象
      var routes = formRouters(param.data.obj);
      console.log("后台返回路由List 转换的结果为:{0}", routes);
      //把转换的结果赋值给 路由路径为/Home 的  children属性
      router.options.routes[1].children = routes;
      console.log("/Home children属性的值为:{0}", router.options.routes[1].children);
    }
  })
}

export const formRouters = (routers) => {
  let array = [];
  routers.forEach(element => {
    let children = [];
    if (element.children && element.children instanceof Array) {
      children = formRouters(element.children);
    }
    let r = {
      path: element.path,
      component(resolve) {
        if (element.component.startsWith('User')) {
          require(['./common/' + element.component + '.vue'], resolve)
        } else if (element.component.startsWith('Role')) {
          require(['./common/' + element.component + '.vue'], resolve)
        }
      },
      name: element.name,
      children: children
    }
    array.push(r);
  });
  return array;
}



new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
