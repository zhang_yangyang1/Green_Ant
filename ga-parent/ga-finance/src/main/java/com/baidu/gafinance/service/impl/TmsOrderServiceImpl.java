package com.baidu.gafinance.service.impl;

import com.baidu.gafinance.entity.TmsOrder;
import com.baidu.gafinance.mapper.TmsOrderMapper;
import com.baidu.gafinance.service.ITmsOrderService;
import com.baidu.vo.ExcelBean;
import com.baidu.vo.ExcelUtils;
import com.baidu.vo.OrderDto;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-29
 */
@Service
public class TmsOrderServiceImpl extends ServiceImpl<TmsOrderMapper, TmsOrder> implements ITmsOrderService {

    @Autowired
    TmsOrderMapper tmsOrderMapper;
    @Override
    public List<OrderDto> selectGetOrder(OrderDto orderDto) {
        return tmsOrderMapper.selectGetOrder(orderDto);
    }

    @Override
    public List<OrderDto> selectGetSort(OrderDto orderDto) {
        return tmsOrderMapper.selectGetSort(orderDto);
    }

    @Override
    public XSSFWorkbook downLoadGoodsExcel(OrderDto orderDto) throws Exception {
        PageHelper.startPage(orderDto.getPageNo(),orderDto.getPageSize());
        Wrapper wrapper = new EntityWrapper();

        List<TmsOrder> list = this.selectList(wrapper);
        PageInfo<TmsOrder> pageInfo = new PageInfo<TmsOrder>(list);
        Map<Integer,List<ExcelBean>> map = new HashMap<Integer,List<ExcelBean>>();
        List<ExcelBean> excelBeans = new ArrayList<>();
        excelBeans.add(new ExcelBean("订单编号","onumber",0));
        excelBeans.add(new ExcelBean("交易时间","odate",0));
//        excelBeans.add(new ExcelBean("类别","bname",0));
        excelBeans.add(new ExcelBean("交易金额","dsum",0));
        excelBeans.add(new ExcelBean("交易类型","otype",0));
        excelBeans.add(new ExcelBean("收支类型","state",0));
        excelBeans.add(new ExcelBean("备注","odesc",0));
        map.put(0,excelBeans);
        return ExcelUtils.createExcelFile(TmsOrder.class,pageInfo.getList(),map,"财务订单列表");
    }

    @Override
    public void updateOne(OrderDto orderDto) {
        tmsOrderMapper.updateOne(orderDto);
    }

    @Override
    public List<OrderDto> selectGetOrderByRece(OrderDto orderDto) {
        return tmsOrderMapper.selectGetOrderByRece(orderDto);
    }
}
