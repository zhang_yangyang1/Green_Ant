package com.baidu.gafinance.service;

import com.baidu.gafinance.entity.TmsOrder;
import com.baidu.vo.OrderDto;
import com.baomidou.mybatisplus.service.IService;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-29
 */
public interface ITmsOrderService extends IService<TmsOrder> {

    /**
     * 多条件查询
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetOrder(OrderDto orderDto);

    /**
     * 多排序
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetSort(OrderDto orderDto);

    /**
     * 导出表格
     * @param orderDto
     * @return
     * @throws Exception
     */
    XSSFWorkbook downLoadGoodsExcel(OrderDto orderDto) throws Exception;


    /**
     * 修改订单状态
     * @param orderDto
     */
    void updateOne(OrderDto orderDto);

    /**
     * 查询用户身份证
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetOrderByRece(OrderDto orderDto);
}
