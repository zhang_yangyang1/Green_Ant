package com.baidu.gafinance.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mht
 * @since 2020-03-29
 */
@Data
@Accessors(chain = true)
@TableName(value = "tms_order")
public class TmsOrder extends Model<TmsOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单号
     */
    private String onumber;

    /**
     * 订单时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date odate;

    /**
     * 线路
     */
    private Integer statementId;

    /**
     * 发货人
     */
    private Integer receId;

    /**
     * 收货人
     */
    private Integer shouId;

    /**
     * 总重量
     */
    private String sumWeight;

    /**
     * 总体积
     */
    private String sumTiji;

    /**
     * 预估运费
     */
    private Double freight;

    /**
     * 是否受理
     */
    private Integer manage;

    /**
     * 是否收货
     */
    private Integer gains;

    /**
     * 是否结算
     */
    private Integer settle;

    /**
     * 发货说明
     */
    private String odesc;

    /**
     * 收支类型
     */
    private Integer state;

    /**
     * 交易金额
     */
    private String dsum;

    /**
     * 交易类型
     */
    private String otype;

    private String sumprice;

    private String identity;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
