package com.baidu.gafinance.mapper;

import com.baidu.gafinance.entity.TmsOrder;
import com.baidu.vo.OrderDto;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.core.annotation.Order;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-29
 */
public interface TmsOrderMapper extends BaseMapper<TmsOrder> {

    /**
     * 多条件查询
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetOrder(OrderDto orderDto);

    /**
     * 查询用户身份证
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetOrderByRece(OrderDto orderDto);

    /**
     * 多排序
     * @param orderDto
     * @return
     */
    List<OrderDto> selectGetSort(OrderDto orderDto);

    /**
     * 修改订单状态
     * @param orderDto
     */
    void updateOne(OrderDto orderDto);
}
