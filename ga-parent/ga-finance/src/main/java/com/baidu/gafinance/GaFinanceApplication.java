package com.baidu.gafinance;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(value = "com.baidu.gafinance.mapper")
public class GaFinanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaFinanceApplication.class, args);
    }

}
