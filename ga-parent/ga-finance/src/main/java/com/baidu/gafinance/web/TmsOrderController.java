package com.baidu.gafinance.web;


import com.baidu.gafinance.service.ITmsOrderService;
import com.baidu.vo.OrderDto;
import com.baidu.vo.ResultEntity;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mht
 * @since 2020-03-29
 */
@RestController
@RequestMapping("/finance/tmsOrder")
@Slf4j
public class TmsOrderController {

    @Autowired
    ITmsOrderService iTmsOrderService;

    /**
     * 财务报表多条件查询
     * @param orderDto
     * @return
     */
    @RequestMapping("/getList")
    public ResultEntity getList(OrderDto orderDto){
        PageHelper.startPage(orderDto.getPageNo(),orderDto.getPageSize());
        List<OrderDto> orderDtos = iTmsOrderService.selectGetOrder(orderDto);
        PageInfo<OrderDto> pageInfo = new PageInfo<>(orderDtos);
        return ResultEntity.ok(pageInfo);
    }
    /**
     * 导出表格
     * @param orderDto
     * @return
     * @throws Exception
     */
    @RequestMapping("/downLoadGoodsExcel")
    public ResponseEntity downLoadGoodsExcel(OrderDto orderDto) throws Exception{
        XSSFWorkbook xSSFWorkbooks = iTmsOrderService.downLoadGoodsExcel(orderDto);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        xSSFWorkbooks.write(byteArrayOutputStream);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentDispositionFormData("attachment", new String("交易订单表.xlsx".getBytes("UTF-8"), "ISO-8859-1"));
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity(byteArrayOutputStream.toByteArray(),httpHeaders, HttpStatus.CREATED);
    }
    @RequestMapping("/getSort")
    public ResultEntity getSort(OrderDto orderDto){
        Wrapper wrapper = new EntityWrapper();
        PageHelper.startPage(orderDto.getPageNo(),orderDto.getPageSize());
        List<OrderDto> orderDtos = iTmsOrderService.selectGetSort(orderDto);
        PageInfo<OrderDto> pageInfo = new PageInfo<>(orderDtos);
        return ResultEntity.ok(pageInfo);
    }
    @RequestMapping("/update")
    public ResultEntity update(OrderDto orderDto){
        iTmsOrderService.updateOne(orderDto);
        return ResultEntity.ok();
    }
    @RequestMapping("/selectList")
    public ResultEntity selectList(OrderDto orderDto){
        PageHelper.startPage(orderDto.getPageNo(),orderDto.getPageSize());
        List<OrderDto> orderDtos = iTmsOrderService.selectGetOrderByRece(orderDto);
        PageInfo<OrderDto> pageInfo = new PageInfo<>(orderDtos);
        return ResultEntity.ok(pageInfo);
    }
}
