package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsCompanyIndustry extends Model<TmsCompanyIndustry> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer companyId;

    private Integer industryId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
