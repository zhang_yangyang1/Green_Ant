package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsStatement extends Model<TmsStatement> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 始发地
     */
    private String sAddress;

    /**
     * 目的地
     */
    private String mAddress;

    /**
     * 运输时间
     */
    private String stime;

    /**
     * 货物价格
     */
    private Double price;

    /**
     * 最低价格
     */
    private Integer sPrice;

    /**
     * 是否支持上门提货
     */
    private Integer ti;

    /**
     * 是否支持送货上门
     */
    private Integer song;

    /**
     * 发布状态
     */
    private Integer state;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
