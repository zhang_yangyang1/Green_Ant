package com.baidu.gatransport.service;

import com.baidu.gatransport.entity.TmsNation;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 城市字典表 服务类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
public interface ITmsNationService extends IService<TmsNation> {

}
