package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsCompany;
import com.baidu.gatransport.mapper.TmsCompanyMapper;
import com.baidu.gatransport.service.ITmsCompanyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsCompanyServiceImpl extends ServiceImpl<TmsCompanyMapper, TmsCompany> implements ITmsCompanyService {

}
