package com.baidu.gatransport.mapper;

import com.baidu.gatransport.entity.TmsCompanyIndustry;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
public interface TmsCompanyIndustryMapper extends BaseMapper<TmsCompanyIndustry> {

}
