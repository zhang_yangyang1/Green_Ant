package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsRece;
import com.baidu.gatransport.mapper.TmsReceMapper;
import com.baidu.gatransport.service.ITmsReceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsReceServiceImpl extends ServiceImpl<TmsReceMapper, TmsRece> implements ITmsReceService {

}
