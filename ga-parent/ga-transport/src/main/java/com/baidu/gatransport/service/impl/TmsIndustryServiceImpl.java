package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsIndustry;
import com.baidu.gatransport.mapper.TmsIndustryMapper;
import com.baidu.gatransport.service.ITmsIndustryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsIndustryServiceImpl extends ServiceImpl<TmsIndustryMapper, TmsIndustry> implements ITmsIndustryService {

}
