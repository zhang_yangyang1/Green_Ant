package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsImage extends Model<TmsImage> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 营业执照
     */
    private String license;

    /**
     * 组织机构代码证
     */
    private String cercode;

    /**
     * 道路运输许可证
     */
    private String permit;

    /**
     * 税务登记证
     */
    private String certificate;

    /**
     * 负责人身份证
     */
    private String mancard;

    /**
     * 关联公司
     */
    private Integer companyId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
