package com.baidu.gatransport.web;


import com.baidu.gatransport.entity.TmsNation;
import com.baidu.gatransport.service.ITmsNationService;
import com.baidu.vo.ResultEntity;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 城市字典表 前端控制器
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@RestController
@RequestMapping("/transport/tmsNation")
public class TmsNationController {

    @Autowired
    ITmsNationService nationService;

    @RequestMapping("/getProvince")
    public ResultEntity getProvince(String pid){
        TmsNation nation = new TmsNation();
        EntityWrapper<TmsNation> entityWrapper = new EntityWrapper<>(nation);
        entityWrapper.eq("pid",pid);
        List<TmsNation> nationList = nationService.selectList(entityWrapper);
        return ResultEntity.ok(nationList);
    }

}
