package com.baidu.gatransport.mapper;

import com.baidu.gatransport.entity.TmsNation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 城市字典表 Mapper 接口
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
public interface TmsNationMapper extends BaseMapper<TmsNation> {

}
