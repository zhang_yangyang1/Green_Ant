package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsNation;
import com.baidu.gatransport.mapper.TmsNationMapper;
import com.baidu.gatransport.service.ITmsNationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 城市字典表 服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsNationServiceImpl extends ServiceImpl<TmsNationMapper, TmsNation> implements ITmsNationService {

}
