package com.baidu.gatransport.web;


import com.baidu.dto.ReceDto;
import com.baidu.gatransport.entity.TmsRece;
import com.baidu.gatransport.service.ITmsReceService;
import com.baidu.vo.ResultEntity;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@RestController
@RequestMapping("/transport/tmsRece")
public class TmsReceController {

    @Autowired
    ITmsReceService receService;

    @RequestMapping("/receList")
    public ResultEntity receList(ReceDto receDto){
        System.out.println(receDto);
        PageHelper.startPage(receDto.getPageNo(),receDto.getPageSize());
        TmsRece rece = new TmsRece();
        EntityWrapper<TmsRece> entityWrapper = new EntityWrapper<>(rece);
        if(StringUtils.isNotBlank(receDto.getName())){
            entityWrapper.like("name",receDto.getName());
        }
        List<TmsRece> receList = receService.selectList(entityWrapper);
        PageInfo<TmsRece> pageInfo = new PageInfo<>(receList);
        return ResultEntity.ok(pageInfo);
    }

    @RequestMapping("/delRece")
    public ResultEntity delRece(int id){
        boolean b = receService.deleteById(id);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

    @RequestMapping("addRece")
    public ResultEntity addRece(ReceDto receDto){
        System.out.println(receDto);
        TmsRece rece = new TmsRece();
        BeanUtils.copyProperties(receDto,rece);
        boolean b = receService.insert(rece);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

    @RequestMapping("/updRece")
    public ResultEntity updRece(ReceDto receDto){
        System.out.println(receDto);
        TmsRece rece = new TmsRece();
        BeanUtils.copyProperties(receDto,rece);
        boolean b = receService.updateById(rece);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

}
