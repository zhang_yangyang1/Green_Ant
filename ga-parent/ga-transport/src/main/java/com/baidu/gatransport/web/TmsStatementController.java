package com.baidu.gatransport.web;


import com.baidu.dto.StatementDto;
import com.baidu.gatransport.entity.TmsStatement;
import com.baidu.gatransport.service.ITmsStatementService;
import com.baidu.vo.ResultEntity;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@RestController
@RequestMapping("/transport/tmsStatement")
public class TmsStatementController {

    @Autowired
    ITmsStatementService statementService;

    @RequestMapping("/StatementList")
    public ResultEntity StatementList(StatementDto statementDto){
        System.out.println(statementDto+"=======================");

        PageHelper.startPage(statementDto.getPageNo(),statementDto.getPageSize());
        TmsStatement statement = new TmsStatement();
        EntityWrapper<TmsStatement> entityWrapper = new EntityWrapper<>(statement);
        if(StringUtils.isNotBlank(statementDto.getMAddress())){
            entityWrapper.like("m_address",statementDto.getMAddress());
        }
        if(StringUtils.isNotBlank(statementDto.getSAddress())){
            entityWrapper.like("s_address",statementDto.getSAddress());
        }
        if(StringUtils.isNotBlank(statementDto.getOrd())){
            if(statementDto.getOrd().equals("1")){
                System.out.println("根据始发地");
                entityWrapper.orderBy("s_address");
            }else if(statementDto.getOrd().equals("2")){
                entityWrapper.orderBy("m_address");
            }else if(statementDto.getOrd().equals("3")){
                entityWrapper.orderBy("s_price");
            }else {
                System.out.println("else");
            }
        }
        List<TmsStatement> statementList = statementService.selectList(entityWrapper);
        PageInfo<TmsStatement> pageInfo = new PageInfo<>(statementList);
        return ResultEntity.ok(pageInfo);
    }

    @RequestMapping("/addStatement")
    public ResultEntity addStatement(StatementDto statementDto){
        System.out.println(statementDto);
        TmsStatement tmsStatement = new TmsStatement();
        BeanUtils.copyProperties(statementDto,tmsStatement);
        boolean b = statementService.insert(tmsStatement);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

    @RequestMapping("/delStatement")
    public ResultEntity delStatement(int id){
        boolean b = statementService.deleteById(id);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

    @RequestMapping("updStatement")
    public ResultEntity updStatement(StatementDto statementDto){
        TmsStatement tmsStatement = new TmsStatement();
        BeanUtils.copyProperties(statementDto,tmsStatement);
        boolean b = statementService.updateById(tmsStatement);
        if(b){
            return ResultEntity.ok(1);
        }
        return ResultEntity.ok(0);
    }

}
