package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsCompanyIndustry;
import com.baidu.gatransport.mapper.TmsCompanyIndustryMapper;
import com.baidu.gatransport.service.ITmsCompanyIndustryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsCompanyIndustryServiceImpl extends ServiceImpl<TmsCompanyIndustryMapper, TmsCompanyIndustry> implements ITmsCompanyIndustryService {

}
