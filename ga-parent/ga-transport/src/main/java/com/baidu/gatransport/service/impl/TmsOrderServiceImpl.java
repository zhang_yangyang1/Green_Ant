package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsOrder;
import com.baidu.gatransport.mapper.TmsOrderMapper;
import com.baidu.gatransport.service.ITmsOrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsOrderServiceImpl extends ServiceImpl<TmsOrderMapper, TmsOrder> implements ITmsOrderService {

}
