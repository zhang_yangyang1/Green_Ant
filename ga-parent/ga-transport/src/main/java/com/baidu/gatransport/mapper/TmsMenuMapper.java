package com.baidu.gatransport.mapper;

import com.baidu.gatransport.entity.TmsMenu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
public interface TmsMenuMapper extends BaseMapper<TmsMenu> {

}
