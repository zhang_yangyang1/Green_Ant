package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsGoods extends Model<TmsGoods> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 货物名称
     */
    private String name;

    /**
     * 件数
     */
    private Integer gnum;

    /**
     * 包装类型
     */
    private String gtype;

    /**
     * 重量
     */
    private Integer gweight;

    /**
     * 体积
     */
    private String volume;

    /**
     * 货物图片
     */
    private String goodsImg;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
