package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsCompany extends Model<TmsCompany> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司全称
     */
    private String name;

    /**
     * 公司简称
     */
    private String jName;

    /**
     * 公司电话
     */
    private String phone;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 官方网站
     */
    private String website;

    /**
     * 办公地址
     */
    private String address;

    /**
     * 详细地址
     */
    private String xAddress;

    /**
     * logo
     */
    private String logo;

    /**
     * 服务宗旨
     */
    private String aim;

    /**
     * 公司介绍
     */
    private String tDesc;

    /**
     * 登录名
     */
    private String username;

    /**
     * 登录密码
     */
    private String password;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
