package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsRece extends Model<TmsRece> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 联系人
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 发货单位
     */
    private String jobname;

    /**
     * 提货地址
     */
    private String address;

    /**
     * 用户余额
     */
    private Double sumprice;

    private String identity;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
