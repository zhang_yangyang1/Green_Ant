package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsImage;
import com.baidu.gatransport.mapper.TmsImageMapper;
import com.baidu.gatransport.service.ITmsImageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsImageServiceImpl extends ServiceImpl<TmsImageMapper, TmsImage> implements ITmsImageService {

}
