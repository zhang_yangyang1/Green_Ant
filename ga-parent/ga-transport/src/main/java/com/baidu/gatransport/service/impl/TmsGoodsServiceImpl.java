package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsGoods;
import com.baidu.gatransport.mapper.TmsGoodsMapper;
import com.baidu.gatransport.service.ITmsGoodsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsGoodsServiceImpl extends ServiceImpl<TmsGoodsMapper, TmsGoods> implements ITmsGoodsService {

}
