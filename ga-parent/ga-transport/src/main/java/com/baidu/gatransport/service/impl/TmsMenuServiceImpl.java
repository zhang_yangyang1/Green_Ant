package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsMenu;
import com.baidu.gatransport.mapper.TmsMenuMapper;
import com.baidu.gatransport.service.ITmsMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsMenuServiceImpl extends ServiceImpl<TmsMenuMapper, TmsMenu> implements ITmsMenuService {

}
