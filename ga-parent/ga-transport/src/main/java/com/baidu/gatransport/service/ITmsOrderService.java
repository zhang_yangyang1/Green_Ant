package com.baidu.gatransport.service;

import com.baidu.gatransport.entity.TmsOrder;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
public interface ITmsOrderService extends IService<TmsOrder> {

}
