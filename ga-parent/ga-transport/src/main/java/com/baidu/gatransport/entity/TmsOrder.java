package com.baidu.gatransport.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Data
@Accessors(chain = true)
public class TmsOrder extends Model<TmsOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单号
     */
    private String onumber;

    /**
     * 订单时间
     */
    private Date odate;

    /**
     * 线路
     */
    private Integer statementId;

    /**
     * 发货人
     */
    private Integer receId;

    /**
     * 收货人
     */
    private Integer shouId;

    /**
     * 总重量
     */
    private String sumWeight;

    /**
     * 总体积
     */
    private String sumTiji;

    /**
     * 预估运费
     */
    private Double freight;

    /**
     * 是否受理
     */
    private Integer manage;

    /**
     * 是否收货
     */
    private Integer gains;

    /**
     * 是否结算
     */
    private Integer settle;

    /**
     * 发货说明
     */
    private String odesc;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
