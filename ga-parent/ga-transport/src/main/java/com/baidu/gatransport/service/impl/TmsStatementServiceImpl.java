package com.baidu.gatransport.service.impl;

import com.baidu.gatransport.entity.TmsStatement;
import com.baidu.gatransport.mapper.TmsStatementMapper;
import com.baidu.gatransport.service.ITmsStatementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WPF
 * @since 2020-04-04
 */
@Service
public class TmsStatementServiceImpl extends ServiceImpl<TmsStatementMapper, TmsStatement> implements ITmsStatementService {

}
