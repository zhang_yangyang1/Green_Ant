package com.baidu.gaorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaOrderApplication.class, args);
    }

}
