package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsCompanyIndustry;
import com.baidu.gabasic.mapper.TmsCompanyIndustryMapper;
import com.baidu.gabasic.service.ITmsCompanyIndustryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@Service
public class TmsCompanyIndustryServiceImpl extends ServiceImpl<TmsCompanyIndustryMapper, TmsCompanyIndustry> implements ITmsCompanyIndustryService {

}
