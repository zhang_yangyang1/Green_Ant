package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsNation;
import com.baidu.gabasic.mapper.TmsNationMapper;
import com.baidu.gabasic.service.ITmsNationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 城市字典表 服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@Service
public class TmsNationServiceImpl extends ServiceImpl<TmsNationMapper, TmsNation> implements ITmsNationService {

}
