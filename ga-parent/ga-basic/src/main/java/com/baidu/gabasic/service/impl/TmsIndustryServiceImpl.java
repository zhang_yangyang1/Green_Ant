package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsIndustry;
import com.baidu.gabasic.mapper.TmsIndustryMapper;
import com.baidu.gabasic.service.ITmsIndustryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@Service
public class TmsIndustryServiceImpl extends ServiceImpl<TmsIndustryMapper, TmsIndustry> implements ITmsIndustryService {

}
