package com.baidu.gabasic.web;


import com.baidu.dto.CompanyDto;
import com.baidu.dto.ImagesDto;
import com.baidu.gabasic.entity.TmsCompany;
import com.baidu.gabasic.entity.TmsCompanyIndustry;
import com.baidu.gabasic.entity.TmsImage;
import com.baidu.gabasic.entity.TmsIndustry;
import com.baidu.gabasic.service.ITmsCompanyIndustryService;
import com.baidu.gabasic.service.ITmsCompanyService;
import com.baidu.gabasic.service.ITmsImageService;
import com.baidu.gabasic.service.ITmsIndustryService;
import com.baidu.gabasic.utils.UploadUtils;
import com.baidu.vo.ResultEntity;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 张洋洋
 * @since 2020-03-24
 */
@RestController
@RequestMapping("/basic/tmsCompany")
@Slf4j
public class TmsCompanyController {

    @Autowired
    ITmsIndustryService industryService;

    @Autowired
    UploadUtils uploadUtils;

    @Autowired
    ITmsCompanyService companyService;

    @Autowired
    ITmsCompanyIndustryService companyIndustryService;

    @Autowired
    ITmsImageService imageService;

    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * 查询优势服务行业集合
     * @return
     */
    @RequestMapping(value = "/getTypeList")
    public ResultEntity getTypeList(){
        Wrapper<TmsIndustry> wrapper = new EntityWrapper<>();
        List<TmsIndustry> tmsIndustries = industryService.selectList(wrapper);
        return ResultEntity.ok(tmsIndustries);
    }


    /**
     * 添加Company基础信息
     * @param companyDto
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "addCompany",method = RequestMethod.POST)
    public ResultEntity addCompany(@ModelAttribute CompanyDto companyDto) throws IOException {
        List<String> upload = uploadUtils.upload(companyDto.getFiles());
        companyDto.setFiles(null);
        if (!upload.isEmpty()){
            companyDto.setLogo(upload.get(0));
        }
        /**
         * 具体业务逻辑
         */
        Integer[] checkList = companyDto.getCheckList();

        TmsCompany company = new TmsCompany();
        BeanUtils.copyProperties(companyDto,company);
        companyService.saveObject(company);

        for (Integer id : checkList){
            TmsCompanyIndustry companyIndustry = new TmsCompanyIndustry();
            companyIndustry.setCompanyId(company.getId());
            companyIndustry.setIndustryId(id);
            companyIndustryService.insert(companyIndustry);
        }
        return ResultEntity.ok();
    }

    /**
     * 添加资质认证信息
     * @param imagesDto
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "addImages",method = RequestMethod.POST)
    public ResultEntity addImages(@ModelAttribute ImagesDto imagesDto) throws IOException {
        List<String> upload1 = uploadUtils.upload(imagesDto.getFiles1());
        imagesDto.setFiles1(null);
        if (!upload1.isEmpty()){
            imagesDto.setLicense(upload1.get(0));
        }
        List<String> upload2 = uploadUtils.upload(imagesDto.getFiles2());
        imagesDto.setFiles2(null);
        if (!upload2.isEmpty()){
            imagesDto.setCercode(upload2.get(0));
        }
        List<String> upload3 = uploadUtils.upload(imagesDto.getFiles3());
        imagesDto.setFiles3(null);
        if (!upload3.isEmpty()){
            imagesDto.setPermit(upload3.get(0));
        }
        List<String> upload4 = uploadUtils.upload(imagesDto.getFiles4());
        imagesDto.setFiles4(null);
        if (!upload4.isEmpty()){
            imagesDto.setCertificate(upload4.get(0));
        }
        List<String> upload5 = uploadUtils.upload(imagesDto.getFiles5());
        imagesDto.setFiles5(null);
        if (!upload5.isEmpty()){
            imagesDto.setMancard(upload5.get(0));
        }
        /**
         * 根据用户名查找用户对象
         */
        String username = imagesDto.getUsername();
        Wrapper<TmsCompany> wrapper = new EntityWrapper<>();
        wrapper.eq("username",username);
        TmsCompany company = companyService.selectOne(wrapper);

        TmsImage image = new TmsImage();
        image.setCompanyId(company.getId());
        image.setLicense(imagesDto.getLicense());
        image.setCercode(imagesDto.getCercode());
        image.setCertificate(imagesDto.getCertificate());
        image.setMancard(imagesDto.getMancard());
        image.setPermit(imagesDto.getPermit());

        imageService.insert(image);

        return ResultEntity.ok();
    }

    /**
     * 修改密码
     * @param company
     * @return
     */
    @RequestMapping(value = "updatePassword")
    public ResultEntity updatePassword(TmsCompany company){
        /**
         * 根据用户名查找用户对象
         */
        String username = company.getUsername();
        String newPassword = passwordEncoder.encode(company.getPassword());
        log.info("----------->"+newPassword);
        company.setPassword(newPassword);
        companyService.updatePassword(company);
        return ResultEntity.ok();
    }


}
