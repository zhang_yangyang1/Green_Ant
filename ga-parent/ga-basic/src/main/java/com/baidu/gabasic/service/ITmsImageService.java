package com.baidu.gabasic.service;

import com.baidu.gabasic.entity.TmsImage;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface ITmsImageService extends IService<TmsImage> {

}
