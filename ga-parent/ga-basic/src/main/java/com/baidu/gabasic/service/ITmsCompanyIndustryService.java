package com.baidu.gabasic.service;

import com.baidu.gabasic.entity.TmsCompanyIndustry;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface ITmsCompanyIndustryService extends IService<TmsCompanyIndustry> {

}
