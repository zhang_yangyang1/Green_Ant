package com.baidu.gabasic.service;

import com.baidu.gabasic.entity.TmsCompany;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface ITmsCompanyService extends IService<TmsCompany> {

    void saveObject(TmsCompany company);

    void updatePassword(TmsCompany company);
}
