package com.baidu.gabasic.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.List;


import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mht
 * @since 2020-03-22
 */
@Data
@Accessors(chain = true)
@TableName(value = "tms_menu")
public class TmsMenu extends Model<TmsMenu> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String icon;

    private String name;

    @TableField("t_id")
    private Integer pId;

    private String path;

    private String component;

    private List<TmsMenu> children;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
