package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsMenu;
import com.baidu.gabasic.mapper.TmsMenuMapper;
import com.baidu.gabasic.service.ITmsMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-22
 */
@Service
public class TmsMenuServiceImpl extends ServiceImpl<TmsMenuMapper, TmsMenu> implements ITmsMenuService {

    @Override
    public List<TmsMenu> getMenuByUserName(String name) {
        return this.baseMapper.getMenuByUserName(name);
    }
}
