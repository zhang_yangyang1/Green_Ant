package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsImage;
import com.baidu.gabasic.mapper.TmsImageMapper;
import com.baidu.gabasic.service.ITmsImageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@Service
public class TmsImageServiceImpl extends ServiceImpl<TmsImageMapper, TmsImage> implements ITmsImageService {

}
