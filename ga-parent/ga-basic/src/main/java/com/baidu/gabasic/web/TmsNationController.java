package com.baidu.gabasic.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 城市字典表 前端控制器
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@RestController
@RequestMapping("/basic/tmsNation")
public class TmsNationController {

}
