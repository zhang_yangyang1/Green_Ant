package com.baidu.gabasic.service.impl;

import com.baidu.gabasic.entity.TmsCompany;
import com.baidu.gabasic.mapper.TmsCompanyMapper;
import com.baidu.gabasic.service.ITmsCompanyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
@Service
public class TmsCompanyServiceImpl extends ServiceImpl<TmsCompanyMapper, TmsCompany> implements ITmsCompanyService {

    @Autowired
    TmsCompanyMapper companyMapper;

    @Override
    public void saveObject(TmsCompany company) {
        companyMapper.saveObject(company);
    }

    @Override
    public void updatePassword(TmsCompany company) {
        companyMapper.updatePassword(company);
    }
}
