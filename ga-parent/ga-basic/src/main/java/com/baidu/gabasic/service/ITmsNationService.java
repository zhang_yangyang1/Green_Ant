package com.baidu.gabasic.service;

import com.baidu.gabasic.entity.TmsNation;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 城市字典表 服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface ITmsNationService extends IService<TmsNation> {

}
