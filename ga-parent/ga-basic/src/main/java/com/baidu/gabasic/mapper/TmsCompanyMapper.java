package com.baidu.gabasic.mapper;

import com.baidu.gabasic.entity.TmsCompany;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface TmsCompanyMapper extends BaseMapper<TmsCompany> {

    void saveObject(TmsCompany company);

    void updatePassword(TmsCompany company);
}
