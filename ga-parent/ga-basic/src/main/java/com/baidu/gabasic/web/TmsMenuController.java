package com.baidu.gabasic.web;


import com.baidu.gabasic.entity.TmsMenu;
import com.baidu.gabasic.service.ITmsMenuService;
import com.baidu.vo.ResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mht
 * @since 2020-03-22
 */
@RestController
@RequestMapping("/basic/tmsMenu")
public class TmsMenuController {
    @Autowired
    ITmsMenuService iTmsMenuService;

    @RequestMapping("getMenuByUserName")
    public ResultEntity getMenuByUserName(Principal member){
        List<TmsMenu> list = iTmsMenuService.getMenuByUserName(member.getName());
        return ResultEntity.ok(list);
    }
}
