package com.baidu.gabasic.mapper;

import com.baidu.gabasic.entity.TmsMenu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-22
 */
public interface TmsMenuMapper extends BaseMapper<TmsMenu> {

    List<TmsMenu> getMenuByUserName(String name);
}
