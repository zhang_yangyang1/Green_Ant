package com.baidu.gabasic.service;

import com.baidu.gabasic.entity.TmsMenu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-22
 */
public interface ITmsMenuService extends IService<TmsMenu> {

    List<TmsMenu> getMenuByUserName(String name);
}
