package com.baidu.gabasic.mapper;

import com.baidu.gabasic.entity.TmsNation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 城市字典表 Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface TmsNationMapper extends BaseMapper<TmsNation> {

}
