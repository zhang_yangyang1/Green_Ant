package com.baidu.gabasic.mapper;

import com.baidu.gabasic.entity.TmsIndustry;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-24
 */
public interface TmsIndustryMapper extends BaseMapper<TmsIndustry> {

}
