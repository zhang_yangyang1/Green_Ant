package com.baidu.vo;

import lombok.Data;

@Data
public class Page {

    private int pageNo;

    private int pageSize;
}
