package com.baidu.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class OrderDto extends Page {

    /**
     * 订单号
     */
    private String onumber;

    /**
     * 订单时间
     */
    private Date odate;

    /**
     * 线路
     */
    @Value("statement_id")
    private Integer statementId;

    /**
     * 发货人
     */
    private Integer receId;

    /**
     * 收货人
     */
    private Integer shouId;

    /**
     * 总重量
     */
    private String sumWeight;

    /**
     * 总体积
     */
    private String sumTiji;

    /**
     * 预估运费
     */
    private Double freight;

    /**
     * 是否受理
     */
    private Integer manage;

    /**
     * 是否收货
     */
    private Integer gains;

    /**
     * 是否结算
     */
    private Integer settle;

    /**
     * 发货说明
     */
    private String odesc;

    /**
     * 收支类型
     */
    private Integer state;

    /**
     * 交易金额
     */
    private String dsum;

    /**
     * 交易类型
     */
    private String otype;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private String sumprice;

    private String identity;
}
