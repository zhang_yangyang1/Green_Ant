package com.baidu.dto;

import lombok.Data;

@Data
public class ReceDto {

    private Integer id;

    /**
     * 联系人
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 发货单位
     */
    private String jobname;

    /**
     * 提货地址
     */
    private String address;

    /**
     * 用户余额
     */
    private Double sumprice;

    private String identity;

    private Integer pageNo=1;
    private Integer pageSize=10;

}
