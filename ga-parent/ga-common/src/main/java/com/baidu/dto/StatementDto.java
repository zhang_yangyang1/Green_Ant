package com.baidu.dto;

import lombok.Data;

@Data
public class StatementDto {

    private Integer id;

    private String ord;

    /**
     * 始发地
     */
    private String sAddress;

    /**
     * 目的地
     */
    private String mAddress;

    /**
     * 运输时间
     */
    private String stime;

    /**
     * 货物价格
     */
    private Double price;

    /**
     * 最低价格
     */
    private Integer sPrice;

    /**
     * 是否支持上门提货
     */
    private Integer ti;

    /**
     * 是否支持送货上门
     */
    private Integer song;

    /**
     * 发布状态
     */
    private Integer state;

    private Integer pageNo=1;

    private Integer pageSize=10;

}
