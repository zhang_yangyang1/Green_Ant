package com.baidu.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CompanyDto {

    /**
     * 公司全称
     */
    private String name;

    /**
     * 公司简称
     */
    private String jName;

    /**
     * 公司电话
     */
    private String phone;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 官方网站
     */
    private String website;

    /**
     * 办公地址
     */
    private String address;

    /**
     * 详细地址
     */
    private String xAddress;

    /**
     * logo
     */
    private String logo;

    /**
     * 服务宗旨
     */
    private String aim;

    /**
     * 公司介绍
     */
    private String tDesc;

    private Integer[] checkList;

    private String[] images;

    private MultipartFile[] files;
}
