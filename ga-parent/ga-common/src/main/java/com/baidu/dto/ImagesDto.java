package com.baidu.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImagesDto {

    /**
     * 营业执照
     */
    private String license;

    /**
     * 组织机构代码证
     */
    private String cercode;

    /**
     * 道路运输许可证
     */
    private String permit;

    /**
     * 税务登记证
     */
    private String certificate;

    /**
     * 负责人身份证
     */
    private String mancard;

    private String username;

    private String[] images;

    private MultipartFile[] files1;

    private MultipartFile[] files2;

    private MultipartFile[] files3;

    private MultipartFile[] files4;

    private MultipartFile[] files5;
}
