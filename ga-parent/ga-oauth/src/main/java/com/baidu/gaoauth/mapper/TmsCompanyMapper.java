package com.baidu.gaoauth.mapper;

import com.baidu.gaoauth.entity.TmsCompany;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2020-03-21
 */
public interface TmsCompanyMapper extends BaseMapper<TmsCompany> {

}
