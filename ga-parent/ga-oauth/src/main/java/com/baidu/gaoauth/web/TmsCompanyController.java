package com.baidu.gaoauth.web;


import com.baidu.vo.ResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mht
 * @since 2020-03-21
 */
@RestController
@RequestMapping("/admin")
public class TmsCompanyController {

    @Autowired
    ConsumerTokenServices consumerTokenServices;

    /**
     * 注销
     * @param access_token
     * @return
     */
    @RequestMapping("/token/logout")
    public ResultEntity logout(String access_token){
        if(consumerTokenServices.revokeToken(access_token)){
            return ResultEntity.ok("注销成功");
        }else {
            return ResultEntity.ok("注销失败");
        }
    }

    @GetMapping("/user/info")
    public ResultEntity user(Principal member) {
        return ResultEntity.ok(member);
    }
}
