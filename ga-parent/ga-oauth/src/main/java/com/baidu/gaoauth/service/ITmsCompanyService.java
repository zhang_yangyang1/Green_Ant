package com.baidu.gaoauth.service;

import com.baidu.gaoauth.entity.TmsCompany;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2020-03-21
 */
public interface ITmsCompanyService extends IService<TmsCompany> {

}
