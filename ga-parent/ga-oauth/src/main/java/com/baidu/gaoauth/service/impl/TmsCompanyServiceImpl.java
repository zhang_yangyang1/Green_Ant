package com.baidu.gaoauth.service.impl;

import com.baidu.gaoauth.entity.TmsCompany;
import com.baidu.gaoauth.mapper.TmsCompanyMapper;
import com.baidu.gaoauth.service.ITmsCompanyService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mht
 * @since 2020-03-21
 */
@Service
@Slf4j
public class TmsCompanyServiceImpl extends ServiceImpl<TmsCompanyMapper, TmsCompany> implements ITmsCompanyService, UserDetailsService {

    @Autowired
    ITmsCompanyService companyService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("用户名：{}",s);
        Wrapper<TmsCompany> tUserWrapper = new EntityWrapper<TmsCompany>();
        tUserWrapper.eq("username",s);
        TmsCompany tmsCompany = companyService.selectOne(tUserWrapper);
        if (tmsCompany==null){
            throw  new UsernameNotFoundException("用户名不正确");
        }
        log.info(passwordEncoder.encode(tmsCompany.getPassword()));
        return new User(s,tmsCompany.getPassword(), AuthorityUtils.createAuthorityList("admin"));
    }
}
