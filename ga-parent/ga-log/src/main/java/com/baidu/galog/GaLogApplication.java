package com.baidu.galog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaLogApplication.class, args);
    }

}
